/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.ox.fucntion;

import java.util.*;

public class OXfunction {
    static int round = 0;
    static String winner = "Draw";
    static boolean isFinish = false;
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static String table[][] = {{"-", "-", "-"},
    {"-", "-", "-"},
    {"-", "-", "-"}};
    static String player = "X";

    public static void showWelcome() {
        System.out.println("------Welcome to ||OX|| Game------");
    }

    public static void showTurn() {
        System.out.println("Turn :" + player);
    }

    public static void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            //debug
            if (table[row][col].equals("-")) {
                table[row][col] = player;
                round+=1;
                break;
            } else {
                System.out.println("Error: table at row and col is not emtry!!! ");
            }
        }
    }

    public static void switchPlayer() {
        if (player.equals("X")) {
            player = "O";
        } else if (player.equals("O")) {
            player = "X";
        }
        
    }

    public static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (!table[row][col].equals(player)) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    public static void checkRol() {
        for (int col = 0; col < 3; col++) {
            if (!table[row][col].equals(player)) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    public static void checkDiagonal(){
        if(table[0][0].equals(player)&&table[1][1].equals(player)&&table[2][2].equals(player)||
           table[0][2].equals(player)&&table[1][1].equals(player)&&table[2][0].equals(player)){
        isFinish = true;
        winner = player;
        }
    }
    
     
     public static void checkWin() {
         checkCol();
         checkRol();
         checkDiagonal();
         
    }

    public static void showResult() {
        showTable();
        System.out.println("The winner is : "+winner);
    }

    public static void showBye() {
        System.out.println("Bye Bye . . . !");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
            if(round==9&&isFinish==false){
                isFinish=true;
            }
        } while (!isFinish);
        showResult();
        showBye();

    }
}
